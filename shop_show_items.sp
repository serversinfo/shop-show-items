#include <shop>

stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_shop_items.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)
public OnPluginStart()
{
	HookEvent("player_spawned", Ev_player_spawned);
	RegConsoleCmd("sm_shopitems", Command_shop_items);
	//HookEvent("player_death",			player_death,			EventHookMode_Pre);
}

//public Action player_death (Event e, const char[] name, bool dontBroadcast)
//{
//	int iClient = GetClientOfUserId(e.GetInt("userid"));
//	char sName[64];
//	dbgMsg("[!shop] Включенные предметы у %N:", iClient);
//	for(int i=1; i<2048; i++)
//		if (Shop_IsItemExists(view_as<ItemId>(i)) && Shop_IsClientItemToggled(iClient, view_as<ItemId>(i))) {
//			Shop_GetItemNameById(view_as<ItemId>(i), sName, sizeof(sName));
//			dbgMsg("%s", sName);
//		}
//}


public Action Command_shop_items(int iClient, int iArgc)
{
	char sName[64];
	int iCount;
	PrintToChat(iClient, "[!shop] Включенные предметы:");
	for(int i=1; i<2048; i++)
		if (Shop_IsItemExists(view_as<ItemId>(i)) && Shop_IsClientItemToggled(iClient, view_as<ItemId>(i))) {
			Shop_GetItemNameById(view_as<ItemId>(i), sName, sizeof(sName));
			PrintToChat(iClient, "%s", sName);
			iCount++;
		}
	if(!iCount)
		PrintToChat(iClient, "[!shop] У вас не включен ни один предмет");
	return Plugin_Continue;
}

public Action Ev_player_spawned(Event event, const char[] name, bool Db) {
	int iClient = GetClientOfUserId(event.GetInt("userid"));
	char sName[64];
	int iCount;
	PrintToChat(iClient, "[!shop] Включенные предметы:");
	for(int i=1; i<2048; i++)
		if (Shop_IsItemExists(view_as<ItemId>(i)) && Shop_IsClientItemToggled(iClient, view_as<ItemId>(i))) {
			Shop_GetItemNameById(view_as<ItemId>(i), sName, sizeof(sName));

			PrintToChat(iClient, "%s", sName);
			iCount++;
		}
	if(!iCount)
		PrintToChat(iClient, "[!shop] У вас не включен ни один предмет");
}
